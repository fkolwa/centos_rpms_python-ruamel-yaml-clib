#! /usr/bin/python3

import sys
import os
from ruamel.yaml import YAML

f = open(os.path.abspath(os.path.join(os.path.dirname(__file__),'..', 'tests.yml')), 'r')
s = f.read()
f.close()

yaml = YAML()
data = yaml.load(s)
if data[0]['hosts'] != 'localhost':
    exit(1)
